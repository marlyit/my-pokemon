// App.js
import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Layout from './components/Layout';
import Header from './components/Header';
import PokemonList from './pages/PokemonList';
import PokemonDetail from './pages/PokemonDetails';

function App() {
  const [search, setSearch] = useState(''); // Déplacer l'état de la recherche ici

  return (
    <Router>
      <Header search={search} setSearch={setSearch} /> {/* Rendre Header ici */}
      <Layout>
        <Routes>
          <Route path="/" exact element={<PokemonList search={search} />} /> {/* Passer l'état de la recherche en tant que props */}
          <Route path="/pokemon/:id" element={<PokemonDetail />} />
        </Routes>
      </Layout>
    </Router>
  );
}

export default App;