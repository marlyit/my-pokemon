// Fichier Footer.js
import React from 'react';

export default function Footer() {
  return (
    <footer className="bg-blue-500 text-white flex items-center justify-center text-center p-4 mt-8">
      <p>© {new Date().getFullYear()} Qwasar Pokemon All Right Reserved</p>
    </footer>
  );
}