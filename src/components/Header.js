// Header.js
import React from 'react';
import { NavLink } from 'react-router-dom';

function Header({ search, setSearch }) {
  const handleSearchChange = (e) => {
    setSearch(e.target.value.toLowerCase());
  };

  return (
    <div>
      <header className='bg-blue-500 flex items-center justify-between px-10 py-6'>
        <NavLink to="/">
          <h1 className='text-2xl text-white font-semibold'>Pokemon</h1>
        </NavLink>
        <nav>
          <ul className='flex items-center gap-10 text-xl text-white'>
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/pokedex">Pokedex</NavLink></li>
            <li><NavLink to="/videogame">Video Game</NavLink></li>
            <li><NavLink to="/news">News</NavLink></li>
          </ul>
        </nav>
        <form className='flex items-center'>
          <input className='px-2 h-[44px] rounded-l' type="text" value={search} onChange={handleSearchChange} />
          <button className='bg-white text-blue-600 font-semibold text-xl px-6 py-2 rounded-r'>Filter</button>
        </form>
      </header>
    </div>
  );
}

export default Header;